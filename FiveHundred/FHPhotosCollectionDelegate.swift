//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** @todo Document this.
    @todo We might need a protocol for "Photos Collection".
 */
protocol FHPhotosCollectionDelegate {
    
    /** Asks the delegate to show the details of a single photo.
      */
    func showDetailsForPhoto (photo : FHPhotoSF)
    
}