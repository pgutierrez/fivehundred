//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The "Features" currently available in 500px, each represents a photo streams.
    While we could use String as the raw value we might want to store this in a database (for example Core Data) and an Int takes much less space.
 */
enum FHFeature : CustomStringConvertible {
    // Global features
    case popular
    case highest_rated
    case upcoming
    case editors
    case fresh_today
    case fresh_yesterday
    case fresh_week
    
    // Per-user features
    case user (userID : String) // <-- a better solution is required for the associated value, it can also be a username */
    case user_friends (userID : String)
    case user_favorites (userID : String)
    
    // MARK: CustomStringConvertible
    
    var description : String {
        switch self {
        case popular:
            return "popular"
        case highest_rated:
            return "highest_rated"
        case upcoming:
            return "upcoming"
        case editors:
            return "editors"
        case fresh_today:
            return "fresh_today"
        case fresh_yesterday:
            return "fresh_yesterday"
        case fresh_week:
            return "fresh_week"
        case user: // <-- @todo: user the associated value
            return "user"
        case user_friends: // <-- @todo: user the associated value
            return "user_friends"
        case user_favorites: // <-- @todo: user the associated value
            return "user_favorites"
        }
    }
}