//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import UIKit
import MapKit

/** Shows the details of a single photo.
    @todo I set the aspect ratio for the image using the known image size, but sometimes it doesn't fit perfectly. We must investigate why.
 */
class FHPhotoDetailController : UIViewController {
    
    // MARK: Nested types
    
    /** Very simple class that conforms to the MKAnnotation protocol.
        A SimpleMapAnnotation will represent a FHPhotoSF in a map view, this was FHPhotoSF doesn't have to comply with the MKAnnotation protocol (which is cumbersome as it requires the class to be a subclass of NSObject).
     */
    class SimpleMapAnnotation : NSObject, MKAnnotation {
        
        // Title
        @objc var title: String? = nil
        
        // Center latitude and longitude of the annotation view.
        var coordinate = CLLocationCoordinate2D (latitude: 0, longitude: 0)
    }
    
    // MARK: Private variables
    
    /** Node for the photo. */
    private var _imageNode : ASNetworkImageNode!
    
    /** Node for the photo owner's avatar. */
    private var _avatarNode : ASNetworkImageNode!
    
    /** Constraint for the top container's aspect ratio. */
    private var _topAspectRatioConstraint : NSLayoutConstraint?
    
    /** Constraint that can "collapse" the bottom container view. */
    private var _bottomCollapseConstraint : NSLayoutConstraint!
     
    // MARK: Private methods
    
    /** Prepares the _imageNode for use.
     */
    private func prepareImageNodes () {
        if _imageNode == nil {
            _imageNode = ASNetworkImageNode ()
            _imageNode.view.translatesAutoresizingMaskIntoConstraints = false
            _imageNode.contentMode = .ScaleAspectFit
            
            imageNodeContainerView.addSubview (_imageNode.view)
            let Vconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("V:|[imageNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["imageNodeView" : _imageNode.view])
            let Hconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("H:|[imageNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["imageNodeView" : _imageNode.view])
            
            imageNodeContainerView.addConstraints (Vconstraints)
            imageNodeContainerView.addConstraints (Hconstraints)
        }
        
        if _avatarNode == nil {
            _avatarNode = ASNetworkImageNode ()
            _avatarNode.view.translatesAutoresizingMaskIntoConstraints = false
            _avatarNode.contentMode = .ScaleAspectFit
            
            avatarNodeContainerView.addSubview (_avatarNode.view)
            let Vconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("V:|[avatarNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["avatarNodeView" : _avatarNode.view])
            let Hconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("H:|[avatarNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["avatarNodeView" : _avatarNode.view])
            
            avatarNodeContainerView.addConstraints (Vconstraints)
            avatarNodeContainerView.addConstraints (Hconstraints)
        }
    }
    
    /** Load's the photo data.
      */
    private func loadPhotoData () {
        guard let photo = datasource?.photo else {
            self.title = NSLocalizedString ("Photo Detail Empty Title", comment: "")
            view.hidden = true
            return
        }
        
        // we don't want to trigger this everytime the view appears, but it will do for the time being
        // in this context we want the largest image available
        _imageNode.URL = photo.images.last!.URL
        nameLabel.text = photo.name
        descriptionLabel.text = photo.textDescription
        _avatarNode.URL = photo.user.avatarURL
        usernameLabel.text = photo.user.fullName
        
        if !photo.camera.hasInfo {
            cameraLabel.text = nil
        } else {
            var cameraAttributes = [String] ()
            if let name = photo.camera.name {
                cameraAttributes.append (name)
            }
            
            if let lens = photo.camera.lens {
                let attributeName = NSLocalizedString ("Camera Attribute - Lens", comment: "")
                cameraAttributes.append ("\(attributeName)\(lens)")
            }
            
            if let focalLength = photo.camera.focalLength {
                let attributeName = NSLocalizedString ("Camera Attribute - Focal Length", comment: "")
                cameraAttributes.append ("\(attributeName)\(focalLength)")
            }
            
            if let ISO = photo.camera.ISO {
                let attributeName = NSLocalizedString ("Camera Attribute - ISO", comment: "")
                cameraAttributes.append ("\(attributeName)\(ISO)")
            }
            
            var cameraDescription = NSLocalizedString ("Camera Attribute - Title", comment: "")
            cameraDescription += cameraAttributes.joinWithSeparator (",")
            cameraLabel.text = cameraDescription
        }
        
        if let coordinate = photo.coordinate {
            let annotation = SimpleMapAnnotation ()
            annotation.title = photo.name
            annotation.coordinate = coordinate
            
            mapView.addAnnotation (annotation)
            mapView.setCenterCoordinate (coordinate, animated: false)
            
            _bottomCollapseConstraint.active = false
        } else {
            _bottomCollapseConstraint.active = true
        }
        
        title = photo.name
        view.hidden = false
        view.setNeedsLayout ()
    }
    
    // MARK: Public attributes
    
    /** Container for the top content of the view, mainly the photo's image.
        At runtime we will add a constraint that will set this view's aspect ratio to that of the photo.
     */
    @IBOutlet
    var topContainerView : UIView!
    
    /** Container for the bottom content of the view, mainly the photo's map.
        At runtime if we don't have the photo's location we will add a constraint that will "collapse" this view, setting its height to zero.
     */
    @IBOutlet
    var bottomContainerView : UIView!
    
    /** Container for the _imageNode. */
    @IBOutlet
    var imageNodeContainerView : UIView!
    
    /** Label for the photo's name. */
    @IBOutlet
    var nameLabel : UILabel!
    
    /** Label for the photo's description. */
    @IBOutlet
    var descriptionLabel : UILabel!
    
    /** Container for the _avatarNode. */
    @IBOutlet
    var avatarNodeContainerView : UIView!
    
    /** Label for the photo owner's name.
      */
    @IBOutlet
    var usernameLabel : UILabel!
    
    /** Label for the camera's information.
      */
    @IBOutlet
    var cameraLabel : UILabel!
    
    /** Map, visible if we have the photo's coordinates.
      */
    @IBOutlet
    var mapView : MKMapView!
    
    /** Source for the photo we will show.
     */
    var datasource : FHPhotoDetailsDataSource?
    
    // MARK: Public methods
    
    /** Tries to load a FHPhotoDetailController from a nib file.
        @param nibName The name of the nib file, without any leading path information. The default is "PhotosCollection".
        @param bundle The bundle in which to search for the nib file. If you specify nil, this method looks for the nib file in the main bundle.
        @return On success a FHPhotoDetailController, otherwise nil.
    */
    class func createPhotoDetailControllerFromNIB (nibName : String = "PhotoDetail", bundle : NSBundle? = nil) -> FHPhotoDetailController? {
        let nib = UINib (nibName: nibName, bundle: bundle)
        let nibContents = nib.instantiateWithOwner (nil, options: nil)
        
        for object in nibContents {
            if let photoDetailController = object as? FHPhotoDetailController {
                return photoDetailController
            }
        }
        
        return nil
    }
    
    /** Quick and dirty hack to reload the photo data on the iPad.
        @todo Fix this!
      */
    func reloadPhotoData () {
        if let _ = view.superview {
            loadPhotoData ()
        }
    }
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareImageNodes ()
        _bottomCollapseConstraint = NSLayoutConstraint (item: bottomContainerView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    }
    
    override func viewWillAppear (animated: Bool) {
        super.viewWillAppear (animated)
        loadPhotoData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        avatarNodeContainerView.layer.cornerRadius = avatarNodeContainerView.bounds.width / 2
        avatarNodeContainerView.clipsToBounds = true
        
        if let photo = datasource?.photo {
            _topAspectRatioConstraint?.active = false
            // this doesn't work as we expect, the aspect ratio of the photo and the image we fetch from the web should be the same... but sometimes they are not
            // @todo fix this!
            
            _topAspectRatioConstraint = NSLayoutConstraint (item: topContainerView, attribute: .Height, relatedBy: .Equal, toItem: topContainerView, attribute: .Width, multiplier: photo.size.height / photo.size.width, constant: 0)
            _topAspectRatioConstraint?.priority = UILayoutPriorityDefaultHigh
            _topAspectRatioConstraint?.active = true
        }
    }
}
