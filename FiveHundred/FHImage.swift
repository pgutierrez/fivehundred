//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** A single image linked to a photo.
    In this context a 'photo' is an object for which several images in different sizes exist.
    @todo Find a better name for this class.
 */
class FHImage {
    
    /** The image's size.
      */
    let size : FHPhotoSize
    
    /** The image's URL.
      */
    let URL : NSURL
    
    /** Designated initializer.
        @param dict An image's short format information.
     */
    init? (dict : NSDictionary) {
        // data validation is left for another day
        let sizeAsInt = dict ["size"] as! Int
        size = FHPhotoSize (rawValue: sizeAsInt)!
        URL = NSURL (string: dict["https_url"] as! String)!
    }
}
