//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The API's endpoints.
    The associated values for each case represent the input parameters for the endpoint.
    For the purposes of this exercise I have not been exhaustive in the parameters supported by each endpoint.
 
    The objective of this enumeration is to define ALL the possible inputs and their "packaging" before they are sent to the backend.
    This is the basic structure I tipically use to access public APIs. This is a rough draft, its main limitation is that it only supports GET endpoints.
 
    To define the accesss to the API we have three components:
    - a FHEndpoint case, it define what parameters are required for fetching data from each API endpoint. It also defines how the paramters are "packaged" (GET/POST)
    - the FH500pxAPI object, it performs the asynchronous request. This object can provide mock data for unit testing.
    - a function that processes the sever's output and produces the data we expect from the endpoint. This is where parsing is handled, and it needs a bit more work to have a better structure. Currently I use extensions to the FHEndpoint enumeration for each of the required functions.
 */
enum FHEndpoint {
    
    // MARK: Cases
    
    /** Requests a page of photos.
      */
    case Photos (feature : FHFeature, onlyFromCategories : [FHCategory], excludeCategories : [FHCategory], sortBy : FHSortOrder, sortDirection : FHSortDirection, page : Int, photoSizes : [FHPhotoSize])
    
    /** Requests a single photo.
     */
    case Photo (photoID : Int, photoSize : FHPhotoSizeRequest)
    
    // MARK: Private variables
    
    /** All endpoints require this payload.
        @remark This payload includes the consumer secret that the backend uses to validate requests.
    */
    private var _defaultPayload : [String : AnyObject] {
        // the keys for this dictionary were defined long ago: the App name, bundle identifier, version, locale, model...
        var dict = [String : AnyObject] ()
        dict ["consumer_key"] = FH500pxAPI.ConsumerSecret
        
        return dict
    }
    
    /** The endpoint's query items.
     */
    var queryItems : [NSURLQueryItem] {
        
        var queryItems = [NSURLQueryItem] ()
        
        switch self {
        case .Photos (let feature, let onlyFromCategories, let excludeCategories, let sortBy, let sortDirection, let page, let photoSizes):
            
            queryItems.append (NSURLQueryItem (name: "feature", value: "\(feature)")) // @todo: support the features that include the User ID
            if !onlyFromCategories.isEmpty {
                queryItems.append (NSURLQueryItem (name: "only", value: onlyFromCategories.map ({$0.description}).joinWithSeparator (",")))
            }
            if !excludeCategories.isEmpty {
                queryItems.append (NSURLQueryItem (name: "exclude", value: excludeCategories.map ({$0.description}).joinWithSeparator (",")))
            }
            
            queryItems.append (NSURLQueryItem (name: "sort", value: "\(sortBy)"))
            queryItems.append (NSURLQueryItem (name: "sort_direction", value: "\(sortDirection)"))
            queryItems.append (NSURLQueryItem (name: "page", value: "\(page)"))
            
            for size in photoSizes {
                queryItems.append (NSURLQueryItem (name: "image_size[]", value: "\(size.rawValue)"))
            }
            
            break
        case .Photo:
            // not used
            break
        }
        
        queryItems.append (NSURLQueryItem (name: "consumer_key", value: FH500pxAPI.ConsumerSecret))
        return queryItems
    }
    
    // MARK: Private methods
    
//    /** Creates a "query" string with the key-value pairs of the dictionary.
//        What we mean by "query" is a string with the pattern: key1=value1&key2=value2&key3=value3...
//        @param dictionary The key-value pairs for the string.
//        @return A "query" string.
//        @todo Review how we should 'escape' strings. The 500px API didn't like the escaped strings I sent in initial testing, but some escaping is required for some parameter types.
//     */
//    private func createQueryItemsWithDictionary (dictionary : [String : AnyObject]) -> [NSURLQueryItem] {
//        var queryItems = [NSURLQueryItem] ()
//        for (key, value) in dictionary {
//            let item = NSURLQueryItem (name: key, value: "\(value)")
//            queryItems.append (item)
//        }
//        
//        return queryItems
//    }
//    
//    /** Appends the default payload to a dictionary.
//        @param dictionary The input dictionary, its values are preserved even if one of its keys is in the _defaultPayload.
//        @return The union of the input and _defaultPayload dictionaries.
//     */
//    private func appendDefaultPayloadToDictionary (dictionary : [String : AnyObject]?) -> [String : AnyObject] {
//        guard let inputDictionary = dictionary else {
//            return _defaultPayload
//        }
//        
//        var output = inputDictionary
//        for (key, value) in _defaultPayload {
//            if !inputDictionary.keys.contains (key) {
//                output [key] = value
//            }
//        }
//        return output
//    }
    
    // MARK: Public methods
    
    /** The endpoint's URL relative to some base URL.
        @param baseURL the base URL for the URL.
        @return The endpoint's URL.
     */
    func URLwithBaseURL (baseURL : NSURL) -> NSURL {
        
        var endpointURLsansParameters : NSURL
        
        switch self {
        case .Photos:
            endpointURLsansParameters = baseURL.URLByAppendingPathComponent ("v1/photos")
        case .Photo:
            endpointURLsansParameters = baseURL.URLByAppendingPathComponent ("v1/photos")
        }
        
        let components = NSURLComponents (URL: endpointURLsansParameters, resolvingAgainstBaseURL: false)!
        components.queryItems = queryItems
        
        return components.URL!
    }
}
