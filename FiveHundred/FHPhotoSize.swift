//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The standard photo sizes.
    @remark Depending on the API's maturity and policy for changes it might NOT make sense to use the pixel size for the case names, but I think the cases should be as "dev-friendly" as possible.
 */
enum FHPhotoSize : Int {
    // Cropped sizes
    
    /* 70x70 pixels */
    case _70x70 = 1
    
    /* 140x140 pixels */
    case _140x140 = 2
    
    /* 280x280 pixels */
    case _280x280 = 3
    
    /* 100x100 pixels */
    case _100x100 = 100
    
    /* 200x200 pixels */
    case _200x200 = 200
    
    /* 400x400 pixels */
    case _440x440 = 440
    
    /* 600x600 pixels */
    case _600x600 = 600
    
    // Uncropped sizes
    
    /* 900 pixels on the longest edge. */
    case _900 = 4
    
    /* 1170 pixels on the longest edge. */
    case _1170 = 5
    
    /* 1080 pixels high. */
    case _1080h = 6
    
    /* 300 pixels high. */
    case _300h = 20
    
    /* 600 pixels high. */
    case _600h = 21
    
    /* 256 pixels on the longest edge. */
    case _256 = 30
    
    /* 1080 pixels on the longest edge. */
    case _1080 = 1080
    
    /* 1600 pixels on the longest edge. */
    case _1600 = 1600
    
    /* 2048 pixels on the longest edge. */
    case _2048 = 2048
}
