//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Extension that defines the function that can process the output of FHEndpoint.Photos.
    @todo Add the error codes that can be thrown when processing the payload
 */
extension FHEndpoint {

    /** Process the output from the Photos payload. On success we will have an array of photos.
        @remark This is a very rough draft, for the time being we don't expect things to fail.
      */
    static func processPhotosReponse (payload : FH500pxAPI.ServerPayload) throws -> [FHPhotoSF] {
        var photos = [FHPhotoSF] ()
        if let photoDictionaries = payload.dictionary ["photos"] as? [NSDictionary] {
            for dict in photoDictionaries {
                if let photo = FHPhotoSF (dict: dict) {
                    photos.append (photo)
                }
            }
        }
        
        return photos
    }
}
