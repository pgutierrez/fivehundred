//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** A single User, created from a photo's "short format".
    Current API documentation does not tell us which attributes are mandatory and which are optional.
    @remark I have not been exhaustive with the photo's attributes.
*/
class FHUserSF {
    
    /** The User's ID. */
    let ID : Int
    
    /** The User's full name. */
    let fullName : String
    
    /** The URL for the User's avatar. */
    let avatarURL : NSURL
    
    /** Designated initializer.
        @param dict A users's short format information.
     */
    init? (dict : NSDictionary) {
        // data validation is left for another day
        // there are many ways to deal with it, but with such a big number of attributes any solution will look "ugly"
        ID = dict ["id"] as! Int
        fullName = dict ["fullname"] as! String
        avatarURL = NSURL (string: dict["userpic_https_url"] as! String)!
    }
}
