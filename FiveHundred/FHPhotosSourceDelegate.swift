//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Objects that implement this protocol will be notified of major events in the life of a FHPhotoSource.
 */
protocol FHPhotosSourceDelegate {
    
    /** The photo source finished refreshing its collection of photos.
        @remark This method will be invoked on an arbitrary queue.
      */
    func photoSourceFinishedRefreshing (source : FHPhotosSource)
    
    /** The photo source could not refresh its colleciton of photos.
        @remark This method will be invoked on an arbitrary queue.
     */
    func photoSourceCouldNotRefresh (source : FHPhotosSource, error : NSError?)
}