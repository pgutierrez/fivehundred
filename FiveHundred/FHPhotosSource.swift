//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Very rough draft for a photos source.
    @todo Add configuration options for the source, currently we can only fetch the photos required for this exercise.
 */
class FHPhotosSource {
    
    // MARK: Private methods.
    
    /** For API access.
     */
    private let _API = FH500pxAPI ()
    
    // MARK: Public attributes
    
    /** The current collection of photos.
     */
    private (set) var photos = [FHPhotoSF] ()
    
    /** The source's delegate.
      */
    var delegate : FHPhotosSourceDelegate? = nil
    
    // Designated initializer.
    init () {
        // for the moment there's nothing more to do
    }
    
    /** Refreshes the photos.
        @todo We need some method to do paging.
      */
    func refresh () {
        // we need square thumbnails
        _API.getPopularPhotos (1, photoSizes: [._440x440, ._1600]) { (photos, error) -> Void in
            
            // simplification: we don't have paging, just a full-blown refresh of ALL the photos
            if let newPhotos = photos where error == nil {
                self.photos = newPhotos
                self.delegate?.photoSourceFinishedRefreshing (self)
            } else {
               self.delegate?.photoSourceCouldNotRefresh (self, error: error)
            }
        }
    }
}
