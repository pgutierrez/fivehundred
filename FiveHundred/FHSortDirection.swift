//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The sort directions.
 */
enum FHSortDirection : CustomStringConvertible {
    
    /* Sort in ascending order (lowest or least-recent first) */
    case Ascending
    
    /* Sort in descending order (highest or most-recent first). This is the default. */
    case Descending
    
    // MARK: CustomStringConvertible
    
    var description : String {
        switch self {
        case .Ascending:
            return "Asc"
        case .Descending:
            return "Desc"
        }
    }
}
