//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import UIKit

/** App's root controller.
    @todo Add support for split-screen iPad.
    @todo Fix issues in the iPhone 6 Plus, where the split view controller in landscape behaves like an iPad.
 */
class FHRootController : UISplitViewController, FHPhotosCollectionDelegate, FHPhotoDetailsDataSource {
    
    // MARK: Private variables
    
    /** The main controller for master view, it contains the photosCollectionController.
     */
    private var _masterNavigationController : UINavigationController!
    
    /** Shows a collection of photos.
      */
    private var _photosCollectionController : FHPhotosCollectionController!
    
    /** Shows the details of a single photo.
      */
    private var _photoDetailController : FHPhotoDetailController!
    
    /** On the iPad this is the main controller for the details view, it contains the _photoDetailController.
        On the iPhone this is not used.
      */
    private var _detailsNavigationController : UINavigationController!
    
    // MARK: UIViewController
    
    /** Designated initializer.
        Loads a FHRootController from the "Root.xib" file.
     */
    init () {
        super.init(nibName: "Root", bundle: nil)
        
        _photosCollectionController = FHPhotosCollectionController.createPhotosCollectionControllerFromNIB ()
        _photosCollectionController?.delegate = self
        assert (_photosCollectionController != nil, "Invalid NIB file for the FHPhotosCollectionController!")
        
        _masterNavigationController = UINavigationController (rootViewController: _photosCollectionController!)
        
        _photoDetailController = FHPhotoDetailController.createPhotoDetailControllerFromNIB ()
        _photoDetailController.datasource = self
        assert (_photosCollectionController != nil, "Invalid NIB file for the FHPhotoDetailController!")
        
        _detailsNavigationController = UINavigationController (rootViewController: _photoDetailController!)
        
        if FHDevice.isiPad {
            preferredDisplayMode = .AllVisible
            viewControllers = [_masterNavigationController, _detailsNavigationController]
        } else {
            viewControllers = [_masterNavigationController]
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: FHPhotosCollectionDelegate
    
    func showDetailsForPhoto (photo : FHPhotoSF) {
        self.photo = photo
        _photoDetailController.reloadPhotoData ()
        
        if FHDevice.isiPad {
            showDetailViewController (_detailsNavigationController, sender: self)
        } else {
            showDetailViewController (_photoDetailController, sender: self)
        }
        
    }
    
    // MARK: FHPhotoDetailsDataSource
    
    var photo : FHPhotoSF!
}