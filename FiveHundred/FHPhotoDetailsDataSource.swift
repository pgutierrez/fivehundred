//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Simple protocol for the datasource of an object that shows the full details of a photo.
 */
protocol FHPhotoDetailsDataSource {
    
    /** The photo to put on display.
      */
    var photo : FHPhotoSF! { get }

}
