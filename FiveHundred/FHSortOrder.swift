//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Options for photo's sorting.
    While we could use String as the raw value we might want to store this in a database (for example Core Data) and an Int takes much less space.
 */
enum FHSortOrder : CustomStringConvertible {
    
    /* Sort by time of upload (note: for a request of a user's favorite photos, this sort order will retrieve the list in the order that the user added photos to their favorites list.) */
    case created_at
    
    /* Sort by rating */
    case rating
    
    /* Sort by the highest rating the photo reached */
    case highest_rating
    
    /* Sort by view count */
    case times_viewed
    
    /* Sort by votes count */
    case votes_count
    
    /* Sort by favorites count */
    case favorites_count
    
    /* Sort by comments count */
    case comments_count
    
    /* Sort by the original date of the image extracted from metadata (might not be available for all images) */
    case taken_at
    
    // MARK: CustomStringConvertible
    
    var description : String {
        switch self {
        case .created_at:
            return "created_at"
        case .rating:
            return "rating"
        case .highest_rating:
            return "highest_rating"
        case .times_viewed:
            return "times_viewed"
        case .votes_count:
            return "votes_count"
        case .favorites_count:
            return "favorites_count"
        case .comments_count:
            return "comments_count"
        case .taken_at:
            return "taken_at"
        }
    }
}
