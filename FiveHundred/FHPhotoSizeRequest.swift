//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The available sizes when requesting photos.
    @remark This is currently unused.
 */
enum FHPhotoSizeRequest : Int {
    
    /** Smallest size avilable. */
    case Smallest = 1
    
    /** Small size. */
    case Small = 2
    
    /** Large size. */
    case Large = 3
    
    /** Largest size. */
    case Largest = 4
}
