//  Created by Pedro Gutiérrez on 16/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** The FHDevice enum tries to determine the type of device we are currently running on.
    We can easily distinguish between an iPhone/iPod touch and an iPad, specific models are trickier because we differentiate based on their screen size which is far from perfect (iPhone 6 Plus' zoom mode).
 */
enum FHDevice {
    
    /** Set to true when the App is running on an iPad, otherwise false.
     */
    static var isiPad : Bool {
        return UIDevice.currentDevice().userInterfaceIdiom == .Pad
    }
    
}
