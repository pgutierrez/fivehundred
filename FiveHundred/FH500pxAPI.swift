//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** Error domain for NSError objects created by FH500pxAPI.
 */
let FH500pxAPIErrorDomain = "com.500px.API.error"

/** The FH500pxAPI encapsulates interactions with the 500px API, and its job is to take a FHEndpoint and perform the basic request to the server, perform basic processing and then ask a 'processor' to parse the data into objects. I tipically prefer POST instead of GET, and this class reflects this. A more detailed look of the 500px API reveals this is not a perfect fit but for the purposes of this exercises I think this solutions is good enough.
    
    We also have NOT designed this with OAuth in mind.
    This class assumes that ALL endpoints return data as a JSON-based dictionary, which is why we can "centralize" the basic data processing in this class.
 */
class FH500pxAPI {
    
    // MARK: Nested types
    
    /** The RequestParameters struct represents the collection of parameters that can modify the requests created by the FH500pxAPI class.
     */
    struct RequestParameters {
        /** Timeout for requests.
         */
        var timeout : NSTimeInterval = 10
        
        /** Cache policy for requests.
            By default we ignore cached data. The caching strategy is something we need to clarify in the future. */
        var cachePolicy : NSURLRequestCachePolicy = .ReloadIgnoringLocalCacheData
        
        /** Set to true to print messages to the console.
            By default we don't print anything to the console.
         */
        var log : Bool = false
    }
    
    /** Error codes for error objects created by FH500pxAPI.
     */
    enum ErrorCode : Int {
        /** The error is unknown.
         Current code cannot create errors with this code, we put it here as a placeholder for future code. */
        case Unknown = -1
        
        /** Data returned by the server is not valid JSON, or it is not a dictionary.
         All endpoints return JSON.
         */
        case InvalidJSONData = 1
    }
    
    /** Simple tuple for what we expect as payload from ALL data tasks.
        @remark We are assuming ALL endpoints return something we can convert to a dictionary.
     */
    typealias ServerPayload = (dictionary : NSDictionary, response : NSHTTPURLResponse)
    
    // MARK: Private variables
    
    /** The default session for all static requests.
     In our current architecture we have a single session object so network tasks share the same configuration and queue, which represents a simpler design that will propably be enough for our needs.
     */
    private var _session : NSURLSession = NSURLSession (configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    
    /** Convenience function that creates a NSURLSessionDataTask that uses a POST request and returns a dictionary object.
        @param endpoint The endpoint to request.
        @param parameters The request's parameters.
        @param handler This block will be invoked with the result of the data task when it finishes: the payload (dictionary and server's HTTP response) or error.
        @return A fully initialized NSURLSessionDataTask that is ready to start working (by invoking its resume function).
        @remark The handler will be invoked on an arbitrary queue.
     */
    private func createDataTaskForEndpoint <OutputType> (endpoint : FHEndpoint, parameters : RequestParameters = RequestParameters (), payloadProcessor : (payload : ServerPayload) throws -> OutputType, handler : (output : OutputType?, error : NSError?) -> Void) -> NSURLSessionDataTask {
        
        let request = NSMutableURLRequest (URL: endpoint.URLwithBaseURL (BaseURL), cachePolicy: parameters.cachePolicy, timeoutInterval: parameters.timeout)
        request.setValue ("utf-8", forHTTPHeaderField:"Accept-Charset")
        request.setValue ("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let dataTask = _session.dataTaskWithRequest (request) { (raw_data : NSData?, raw_response : NSURLResponse?, raw_error : NSError?) in
            
            guard let data = raw_data, let response = raw_response as? NSHTTPURLResponse else {
                handler (output: nil, error: raw_error)
                return
            }
            
            if parameters.log {
                if let string = data.asStringWithEncoding (NSUTF8StringEncoding) {
                    debugPrint ("Endpoint \(endpoint) response: '\(string)'")
                } else {
                    debugPrint ("Endpoint \(endpoint) response: '\(data.length) bytes")
                }
            }
            
            // at this level we only care if the data represents a JSON-defined dictionary
            do {
                if let dict = try NSJSONSerialization.JSONObjectWithData (data, options: NSJSONReadingOptions ()) as? NSDictionary {
                    let output = try payloadProcessor (payload: (dictionary : dict, response : response))
                    handler (output: output, error: nil)
                    return
                }
            } catch {
                // let error = NSError (domain: endpoint.ErrorDomain, code: endpointError.in, userInfo: <#T##[NSObject : AnyObject]?#>)
            }
            
            // if we get this far it means data is NOT a valid JSON-defined dictionary
            let error = NSError (domain: FH500pxAPIErrorDomain, code: ErrorCode.InvalidJSONData.rawValue, userInfo: nil)
            handler (output: nil, error: error)
            
        }
        
        return dataTask
    }
    
    // MARK: Public attributes
    
    /** Base URL for all requests.
        We assume that we will always have different endpoints with the same base URL.
     */
    let BaseURL = NSURL (string: "https://api.500px.com/")!
    
    /* The App's consumer secret .*/
    static let ConsumerSecret = _500pxConsumerKey
    
    // MARK: Public methods
    
    /** Convenience method for the current excersize.
     */
    func getPopularPhotos (page : Int, photoSizes : [FHPhotoSize], handler : (photos : [FHPhotoSF]?, error : NSError?) -> Void) {
        getPhotos (FHFeature.popular, onlyFromCategories: [], excludeCategories: [], sortBy: .highest_rating, sortDirection: .Descending, page: page, photoSizes: photoSizes, handler: handler)
    }
    
    // @todo Document this function
    func getPhotos (feature : FHFeature, onlyFromCategories : [FHCategory], excludeCategories : [FHCategory], sortBy : FHSortOrder, sortDirection : FHSortDirection, page : Int, photoSizes : [FHPhotoSize], handler : (photos : [FHPhotoSF]?, error : NSError?) -> Void) {
        // data validation is left for the future, we simply package the inputs in the FHEndpoint and issue a request
        let endpoint = FHEndpoint.Photos (feature: feature, onlyFromCategories: onlyFromCategories, excludeCategories: excludeCategories, sortBy: sortBy, sortDirection: sortDirection, page: page, photoSizes: photoSizes)
        let dataTask = createDataTaskForEndpoint (endpoint, payloadProcessor: FHEndpoint.processPhotosReponse, handler: handler)
        dataTask.resume()
    }
}
