//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import UIKit

/** Quick and dirty implementation of the view controller that shows the collection of popular photos.
 */
class FHPhotosCollectionController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FHPhotosSourceDelegate {
    
    // MARK: Private variables
    
    /** Source of the photos.
     */
    let _photosSource : FHPhotosSource
    
    // MARK: Public attributes
    
    /** The collection view that shows the photos.
     */
    @IBOutlet
    var photosCollectionView : UICollectionView!
    
    /** Our delegate, what we need to ask other parts of the App to act on the photos we manage.
      */
    var delegate : FHPhotosCollectionDelegate?
    
    // MARK: Public methods
    
    @IBAction
    func showMe () {

    }
    
    /** Tries to load a FHPhotosCollectionController from a nib file.
        @param nibName The name of the nib file, without any leading path information. The default is "PhotosCollection".
        @param bundle The bundle in which to search for the nib file. If you specify nil, this method looks for the nib file in the main bundle.
        @return On success a FHPhotosCollectionController, otherwise nil.
     */
    class func createPhotosCollectionControllerFromNIB (nibName : String = "PhotosCollection", bundle : NSBundle? = nil) -> FHPhotosCollectionController? {
        let nib = UINib (nibName: nibName, bundle: bundle)
        let nibContents = nib.instantiateWithOwner (nil, options: nil)
        
        for object in nibContents {
            if let photosCollectionController = object as? FHPhotosCollectionController {
                return photosCollectionController
            }
        }
        
        return nil
    }
    
    // MARK: UIViewController
    
    required init? (coder aDecoder: NSCoder) {
        _photosSource = FHPhotosSource ()
        super.init(coder: aDecoder)
        
        _photosSource.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photosCollectionView.registerNib (FHPhotosCollectionCell.NIB, forCellWithReuseIdentifier: FHPhotosCollectionCell.ReuseIdentifier)
    }
    
    override func viewWillAppear (animated: Bool) {
        super.viewWillAppear (animated)
        
        // we don't want to trigger this everytime the view appears, but it will do for the time being
        _photosSource.refresh()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize (size, withTransitionCoordinator: coordinator)
        
        photosCollectionView.collectionViewLayout.invalidateLayout ()
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView (collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _photosSource.photos.count
    }
    
    func collectionView (collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = photosCollectionView.dequeueReusableCellWithReuseIdentifier (FHPhotosCollectionCell.ReuseIdentifier, forIndexPath: indexPath) as! FHPhotosCollectionCell
        cell.photo = _photosSource.photos [indexPath.row]
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView (collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        delegate?.showDetailsForPhoto (_photosSource.photos [indexPath.row])
        
        return false
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView (collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // quick-and-dirty way to have a two-column collection
        let cell_width = 0.48 * photosCollectionView.bounds.width
        
        return CGSize (width: cell_width, height: cell_width)
    }
    
    // MARK: FHPhotosSourceDelegate
    
    func photoSourceFinishedRefreshing (source : FHPhotosSource) {
        dispatch_async (dispatch_get_main_queue()) {
            self.photosCollectionView.reloadData ()
        }
    }
    
    /**
        @todo Implement this.
      */
    func photoSourceCouldNotRefresh (source : FHPhotosSource, error : NSError?) {
        
    }
}
