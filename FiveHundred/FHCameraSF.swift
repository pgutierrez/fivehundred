//
//  FHCamera.swift
//  FiveHundred
//
//  Created by Pedro Gutiérrez on 16/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.
//

import Foundation

/** Information about the camera, created from a photo's "short format".
    Current API documentation does not tell us which attributes are mandatory and which are optional.
    @remark I have not been exhaustive with the photo's attributes.
 */
class FHCameraSF {
    
    /** The camera's name. */
    let name : String?
    
    /** The camera's lens. */
    let lens : String?
    
    /** The camera's focal length. */
    let focalLength : String?
    
    /** The camera's ISO setting. */
    let ISO : String?
    
    /** The camera's shutter speed. */
    let shutterSpeed : String?
    
    /** The camera's aperture. */
    let aperture : String?
    
    /** True if at least one attribute is non-nil. */
    var hasInfo : Bool {
        return ( name != nil || lens != nil || focalLength != nil || ISO != nil || shutterSpeed != nil || aperture != nil )
    }

    /** Designated initializer.
        @param dict An camera's short format information.
     */
    init (dict : NSDictionary) {
        // data validation is left for another day
        name = dict ["camera"] as? String
        lens = dict ["lens"] as? String
        focalLength = dict ["focal_length"] as? String
        ISO = dict ["iso"] as? String
        shutterSpeed = dict ["shutter_speed"] as? String
        aperture = dict ["aperture"] as? String
    }
}