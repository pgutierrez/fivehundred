//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import UIKit
import QuartzCore

/** Simple subclass that shows a single FHPhotoSF object...
 */
class FHPhotosCollectionCell : UICollectionViewCell {
    
    // MARK: Private variables
    
    private var _imageNode : ASNetworkImageNode!
    
    // MARK: Private methods
    
    /** Prepares the _imageNode for use.
     */
    private func prepareImageNode () {
        if _imageNode == nil {
            _imageNode = ASNetworkImageNode ()
            _imageNode.view.translatesAutoresizingMaskIntoConstraints = false
            _imageNode.contentMode = .ScaleAspectFit
            
            imageNodeContainerView.addSubview (_imageNode.view)
            let Vconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("V:|[imageNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["imageNodeView" : _imageNode.view])
            let Hconstraints = NSLayoutConstraint.constraintsWithVisualFormat ("H:|[imageNodeView]|", options: NSLayoutFormatOptions (), metrics: nil, views: ["imageNodeView" : _imageNode.view])
            
            imageNodeContainerView.addConstraints (Vconstraints)
            imageNodeContainerView.addConstraints (Hconstraints)
        }
    }
    
    // MARK: Public attributes
    
    /* The default NIB for the FHPhotosCollectionCell. */
    static let NIB = UINib (nibName: "PhotosCollectionCell", bundle: nil)
    
    /* The default cell reuse identifier. */
    static let ReuseIdentifier = "PhotosCollectionCell"
    
    /** Container for the _imageNode. */
    @IBOutlet
    var imageNodeContainerView : UIView!
    
    /** Label for the photo's name. */
    @IBOutlet
    var nameLabel : UILabel!
    
    /** Label for the photo's rating. */
    @IBOutlet
    var ratingLabel : UILabel!
    
    var photo : FHPhotoSF? {
        didSet {
            if let p = photo {
                prepareImageNode ()
                
                // in this context we want the smallest image available
                _imageNode.URL = p.images.first!.URL
                nameLabel.text = p.name
                ratingLabel.text = String (format: "%.0f", p.rating)
                ratingLabel.superview?.layer.cornerRadius = (ratingLabel.intrinsicContentSize().width + 4) / 2
                ratingLabel.superview?.clipsToBounds = true
            }
        }
    }
}
