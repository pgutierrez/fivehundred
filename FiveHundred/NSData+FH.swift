//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

extension NSData {
    
    // MARK: Convenience
    
    /** Tries to create a string with the receiver's bytes.
        @param encoding The encoding used by the receiver.
        @return The string representation of the data or nil if the string could not be initialized (for example, if the receiver's data does not represent something valid for the given encoding).
     */
    func asStringWithEncoding (encoding : NSStringEncoding) -> String? {
        return NSString (data: self, encoding: encoding) as? String
    }
}
