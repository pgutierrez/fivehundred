//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

extension String {
    
    /** The default stringByAddingPercentEncoding method does not escape some characters that some backend services expect to be escaped, that's why this method is very aggresive with the percent encoding: it will only leave letters unescaped.
        @remark This is equal to invoking stringByAddingPercentEncodingWithAllowedCharacters with NSCharacterSet.letterCharacterSet() as the set, and assuming we will never get a nil.
     */
    var stringByAddingPercentEncoding : String {
        let set = NSCharacterSet.letterCharacterSet()
        
        return stringByAddingPercentEncodingWithAllowedCharacters (set)!
    }
}
