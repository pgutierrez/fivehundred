//  Created by Pedro Gutiérrez on 15/11/15.
//  Copyright © 2015 AppMonkey. All rights reserved.

import Foundation

/** A single photo, created from a "short format".
    Current API documentation does not tell us which attributes are mandatory and which are optional.
    @remark I have not been exhaustive with the photo's attributes.
 */
class FHPhotoSF : CustomStringConvertible {
    
    // MARK: Private variables
    
    // MARK: Public attributes
    
    /* ID of the photo. */
    let ID : Int
    
    /* Title of the photo */
    let name : String
    
    /* The current rating [0, 100]. */
    let rating : Float
    
    /* The photo's description, if any. */
    let textDescription : String?
    
    /* The different images of the photo, sorted by size (smallest first). */
    let images : [FHImage]
    
    /* The photo's size, in pixels. */
    let size : CGSize
    
    /* The photo's location, if available. */
    let coordinate : CLLocationCoordinate2D?
    
    /* The photo's owner. */
    let user : FHUserSF
    
    /* Information, if any, about the camera used to take the photo. */
    let camera : FHCameraSF
    
    /** Designated initializer.
        @param dict A photo's short format information.
      */
    init? (dict : NSDictionary) {
        // data validation is left for another day
        // there are many ways to deal with it, but with such a big number of attributes any solution will look "ugly"
        
        ID = dict ["id"] as! Int
        name = dict ["name"] as! String
        rating = dict ["rating"] as! Float
        textDescription = dict ["description"] as? String
        
        // we can get any number of image sizes
        var mImages = [FHImage] ()
        let imageDictionaries = dict ["images"] as! [NSDictionary]
        for imageDict in imageDictionaries {
            if let image = FHImage (dict: imageDict) {
                mImages.append (image)
            }
        }
        
        images = mImages.sort ({$0.size.rawValue < $1.size.rawValue})
        
        let width = dict ["width"] as! CGFloat
        let height = dict ["height"] as! CGFloat
        size = CGSize (width: width, height: height)
        
        if let latitude = dict ["latitude"] as? CLLocationDegrees, let longitude = dict ["longitude"] as? CLLocationDegrees {
            coordinate = CLLocationCoordinate2D (latitude: latitude, longitude: longitude)
        } else {
            coordinate = nil
        }
        
        user = FHUserSF (dict: dict ["user"] as! NSDictionary)!
        camera = FHCameraSF (dict: dict)
    }
    
    // MARK: CustomStringConvertible
    
    var description : String {
        return "Photo (Short-Format) \(ID)"
    }
}
