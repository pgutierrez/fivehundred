# README #

You might **love** markdown... but I usually just write text files.

### What is this repository for? ###

Your test :)
A quick demo of how to access the 500px API to show a collection of images and their details.

### Dependencies ###

* AsyncDisplayKit (http://asyncdisplaykit.org) to load the images asynchronously.

### Notes ###

* Didn't use CoreData for two reasons: first, I didn't want to invest more time in this (I started late on Sunday); second, the current data model is too simple, adding CoreData would be superfluous.

### Major problems found ###

It has been a long time since I've developed without a single Storyboard. It took me a while to remember how to create the Main.xib and start from there. Then it was a matter of dealing with the differences between Storyboards and plain XIB files.

### Known issues ###

There are a few @todos in the code, but the worst known issues are:
* I simply skipped lots of data validation and error management.
* Doesn't work as expected on the iPhone 6 Plus in landscape because the split view controller behaves just like if it were running on an iPad, something I didn't account for.
* Switching between portrait/landscape needs more work. 
* Data is loaded when a view appears, this is clearly *not the way to do it*.